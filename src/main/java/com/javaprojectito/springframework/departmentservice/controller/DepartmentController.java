package com.javaprojectito.springframework.departmentservice.controller;

import com.javaprojectito.springframework.departmentservice.dto.DepartmentDto;
import com.javaprojectito.springframework.departmentservice.service.DepartmentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@AllArgsConstructor
@RestController
@RequestMapping("api/department")
public class DepartmentController {
    private DepartmentService departmentService;

    @PostMapping("created")
    public ResponseEntity<DepartmentDto> createdDept(@RequestBody DepartmentDto department){
        DepartmentDto departmentDto = departmentService.createDept(department);
        return new ResponseEntity<>(departmentDto, HttpStatus.CREATED);
    }

    @GetMapping("list")
    public ResponseEntity<List<DepartmentDto>> getAllDept(){
        return new ResponseEntity<>(departmentService.getAll(), HttpStatus.OK);
    }

    @GetMapping("by/{code}")
    public ResponseEntity<DepartmentDto> getById(@PathVariable String code){
        return new ResponseEntity<>(departmentService.getByCodeDept(code), HttpStatus.OK);
    }

    @PutMapping("update/{code}")
    public ResponseEntity<DepartmentDto> updateDept(@PathVariable String code, @RequestBody DepartmentDto departmentDto){
        return new ResponseEntity<>(departmentService.updateDept(code,departmentDto), HttpStatus.OK);
    }

    @DeleteMapping("delete/{code}")
    public void deleteDept(@PathVariable String code){
        departmentService.deleteDept(code);
    }
}
