package com.javaprojectito.springframework.departmentservice.repository;

import com.javaprojectito.springframework.departmentservice.entity.DepartmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;



public interface DepartmentRepository extends JpaRepository<DepartmentEntity, String> {
    boolean existsByDepartmentName(String departmentName);
    boolean existsByContact(String contact);
    boolean existsByEmail(String email);
    boolean existsByDepartmentNameAndEmailAndContact(String departmentName,String email, String contact);
    boolean existsByContactAndEmail(String contact, String email);
    boolean existsByContactAndDepartmentName(String contact, String departmentName);
    boolean existsByEmailAndDepartmentName(String email, String departmentName);
}
