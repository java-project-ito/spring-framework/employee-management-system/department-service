package com.javaprojectito.springframework.departmentservice.service.impl;

import com.javaprojectito.springframework.departmentservice.dto.DepartmentDto;
import com.javaprojectito.springframework.departmentservice.exception.badRequest.invalidformat.ContactInvalidFormatHandler;
import com.javaprojectito.springframework.departmentservice.service.InvalidFormatService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class InvalidFormatServiceImpl implements InvalidFormatService {
    @Override
    public void errorContactInvalidFormat(DepartmentDto departmentDto) {
        String format = "0123456789";
        int counter = 0;
        for(String s:departmentDto.getContact().split("")){
            for (String str : format.split("")){
                if (str.equals(s)){
                    counter ++;
                    break;
                }
            }
        }

        if(counter!=departmentDto.getContact().length()){
            throw new ContactInvalidFormatHandler(HttpStatus.BAD_REQUEST.toString());

        }

    }
}
