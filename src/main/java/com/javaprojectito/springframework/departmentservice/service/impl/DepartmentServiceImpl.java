package com.javaprojectito.springframework.departmentservice.service.impl;

import com.javaprojectito.springframework.departmentservice.dto.DepartmentDto;
import com.javaprojectito.springframework.departmentservice.entity.DepartmentEntity;
import com.javaprojectito.springframework.departmentservice.entity.generatorContact.FormattedCountryCode;
import com.javaprojectito.springframework.departmentservice.entity.generatorCountry.CapitalizeWords;
import com.javaprojectito.springframework.departmentservice.entity.generatorId.CustomIdGenerator;
import com.javaprojectito.springframework.departmentservice.exception.badRequest.datanotfound.NotFoundHandler;
import com.javaprojectito.springframework.departmentservice.repository.DepartmentRepository;
import com.javaprojectito.springframework.departmentservice.service.DataAlreadyExistsService;
import com.javaprojectito.springframework.departmentservice.service.DataNullService;
import com.javaprojectito.springframework.departmentservice.service.DepartmentService;
import com.javaprojectito.springframework.departmentservice.service.UpdateService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@AllArgsConstructor
@Service
public class DepartmentServiceImpl implements DepartmentService {
    private DepartmentRepository departmentRepository;
    private DataAlreadyExistsService dataAlreadyExistsService;
    private InvalidFormatServiceImpl invalidFormatService;
    private DataNullService dataNullService;
    private UpdateService updateService;
    @Override
    public DepartmentDto createDept(DepartmentDto department) {

        //nullchecking
        dataNullService.errorDataNull(department);

        //error checking
        dataAlreadyExistsService.errorDataAlreadyExist(department);
        invalidFormatService.errorContactInvalidFormat(department);

        //Instance object
        CustomIdGenerator customIdGenerator =new CustomIdGenerator();
        CapitalizeWords capitalizeWords = new CapitalizeWords();
        FormattedCountryCode formattedCountryCode = new FormattedCountryCode();

        DepartmentEntity departmentEntity = new DepartmentEntity();

        //convert dto to entity
        departmentEntity.setDepartmentName(capitalizeWords.capitalizeWords(department.getDepartmentName()));
        departmentEntity.setId(customIdGenerator.idDept(departmentEntity.getDepartmentName()));
        departmentEntity.setEmail(department.getEmail().toLowerCase());
        departmentEntity.setCountry(capitalizeWords.capitalizeWords(department.getCountry().toLowerCase()));
        departmentEntity.setContact(formattedCountryCode.contactFormatted(departmentEntity.getCountry(), department.getContact()));


        departmentRepository.save(departmentEntity);

        DepartmentDto departmentDto = new DepartmentDto();

        //convert entity to dto
        departmentDto.setDepartmentName(departmentEntity.getDepartmentName());
        departmentDto.setId(departmentEntity.getId());
        departmentDto.setEmail(departmentEntity.getEmail().toLowerCase());
        departmentDto.setContact(departmentEntity.getContact());
        departmentDto.setCountry(departmentEntity.getCountry());

        return departmentDto;

    }

    @Override
    public List<DepartmentDto> getAll(){
        List<DepartmentDto> departmentDtoList = new ArrayList<>();

        for(DepartmentEntity departmentEntity : departmentRepository.findAll()){
            DepartmentDto departmentDto = new DepartmentDto(
                    departmentEntity.getId(),
                    departmentEntity.getDepartmentName(),
                    departmentEntity.getEmail(),
                    departmentEntity.getContact(),
                    departmentEntity.getCountry()
            );
            departmentDtoList.add(departmentDto);
        }

        return departmentDtoList;
    }

    @Override
    public DepartmentDto getByCodeDept(String string){
        DepartmentEntity departmentDto = departmentRepository.findById(string)
                .orElseThrow(()->new NotFoundHandler("id", string));

        return new DepartmentDto(
                departmentDto.getId(),
                departmentDto.getDepartmentName(),
                departmentDto.getEmail(),
                departmentDto.getContact(),
                departmentDto.getCountry()
        );
    }

    @Override
    public DepartmentDto updateDept(String codeDept, DepartmentDto departmentDto){
        DepartmentEntity departmentEntity = departmentRepository.findById(codeDept).orElseThrow(
                ()->new NotFoundHandler("Id", codeDept)
        );

        //Instance object
        CapitalizeWords capitalizeWords = new CapitalizeWords();
        FormattedCountryCode formattedCountryCode = new FormattedCountryCode();


        //convert dto to entity
        departmentEntity.setDepartmentName(capitalizeWords.capitalizeWords(updateService.checkNameDept(departmentDto, departmentEntity)));
        departmentEntity.setId(codeDept);
        departmentEntity.setEmail(updateService.checkEmailDept(departmentDto, departmentEntity).toLowerCase());
        departmentEntity.setCountry(capitalizeWords.capitalizeWords(updateService.checkCountryDept(departmentDto,departmentEntity).toLowerCase()));
        departmentEntity.setContact(formattedCountryCode.contactFormatted(departmentEntity.getCountry(), updateService.checkContactDept(departmentDto, departmentEntity)));


        departmentRepository.save(departmentEntity);

        return new DepartmentDto(
                departmentEntity.getId(),
                departmentEntity.getDepartmentName(),
                departmentEntity.getEmail(),
                departmentEntity.getContact(),
                departmentEntity.getCountry()
        );
    }

    @Override
    public void deleteDept(String codeDepartment) {
        DepartmentEntity departmentEntity = departmentRepository.findById(codeDepartment).orElseThrow(
                ()->new NotFoundHandler("Id", codeDepartment)
        );
        departmentRepository.delete(departmentEntity);
    }
}
