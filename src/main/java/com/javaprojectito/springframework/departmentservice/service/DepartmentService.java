package com.javaprojectito.springframework.departmentservice.service;

import com.javaprojectito.springframework.departmentservice.dto.DepartmentDto;

import java.util.List;


public interface DepartmentService {
    DepartmentDto createDept(DepartmentDto department);

    List<DepartmentDto> getAll();
    DepartmentDto getByCodeDept(String codeDepartment);

    DepartmentDto updateDept(String codeDepartment, DepartmentDto departmentDto);

    void deleteDept(String codeDepartment);
}
