package com.javaprojectito.springframework.departmentservice.service;

import com.javaprojectito.springframework.departmentservice.dto.DepartmentDto;

public interface InvalidFormatService {
    void errorContactInvalidFormat(DepartmentDto departmentDto);

}
