package com.javaprojectito.springframework.departmentservice.service.impl;

import com.javaprojectito.springframework.departmentservice.dto.DepartmentDto;
import com.javaprojectito.springframework.departmentservice.exception.badRequest.dataalreadyexist.*;
import com.javaprojectito.springframework.departmentservice.repository.DepartmentRepository;
import com.javaprojectito.springframework.departmentservice.service.DataAlreadyExistsService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class DataAlreadyExistImpl implements DataAlreadyExistsService {
    private DepartmentRepository departmentRepository;
    @Override
    public void errorDataAlreadyExist(DepartmentDto department) {
        if(departmentRepository.existsByDepartmentNameAndEmailAndContact(department.getDepartmentName(), department.getEmail(), department.getContact())){
            throw new AllDataAlreadyExistHandler(HttpStatus.BAD_REQUEST.toString());
        }
        if(departmentRepository.existsByContactAndDepartmentName(department.getContact(), department.getDepartmentName())){
            throw new ContactAndNameAlreadyExistHandler(HttpStatus.BAD_REQUEST.toString());
        }
        if(departmentRepository.existsByContactAndEmail(department.getContact(), department.getEmail())){
            throw new ContactAndEmailAlreadyExistHandler(HttpStatus.BAD_REQUEST.toString());
        }
        if (departmentRepository.existsByEmailAndDepartmentName(department.getEmail(), department.getDepartmentName())){
            throw new EmailAndNameAlreadyExistHandler(HttpStatus.BAD_REQUEST.toString());
        }
        if(departmentRepository.existsByDepartmentName(department.getDepartmentName())){
            throw new NameAlreadyExistHandler(HttpStatus.BAD_REQUEST.toString());
        }
        if(departmentRepository.existsByEmail(department.getEmail())){
            throw new EmailAlreadyExistHandler(HttpStatus.BAD_REQUEST.toString());
        }
        if(departmentRepository.existsByContact(department.getContact())){
            throw new ContactAlreadyExistHandler(HttpStatus.BAD_REQUEST.toString());
        }
    }
}
