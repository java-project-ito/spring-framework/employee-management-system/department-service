package com.javaprojectito.springframework.departmentservice.service;

import com.javaprojectito.springframework.departmentservice.dto.DepartmentDto;
import com.javaprojectito.springframework.departmentservice.entity.DepartmentEntity;

public interface UpdateService {
    String checkNameDept(DepartmentDto departmentDto, DepartmentEntity departmentEntity);
    String checkEmailDept(DepartmentDto departmentDto, DepartmentEntity departmentEntity);
    String checkContactDept(DepartmentDto departmentDto, DepartmentEntity departmentEntity);
    String checkCountryDept(DepartmentDto departmentDto, DepartmentEntity departmentEntity);
}
