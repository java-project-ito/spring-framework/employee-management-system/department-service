package com.javaprojectito.springframework.departmentservice.service.impl;

import com.javaprojectito.springframework.departmentservice.dto.DepartmentDto;
import com.javaprojectito.springframework.departmentservice.entity.DepartmentEntity;
import com.javaprojectito.springframework.departmentservice.exception.badRequest.datanull.*;
import com.javaprojectito.springframework.departmentservice.service.DataNullService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class DataNullServiceImpl implements DataNullService {

    @Override
    public void errorDataNull(DepartmentDto department) {

        if(department.getDepartmentName()==null){
            throw new NameNullHandler(HttpStatus.BAD_REQUEST.toString());
        }
        if(department.getEmail() == null){
            throw new EmailNullHandler(HttpStatus.BAD_REQUEST.toString());
        }
        if(department.getContact()==null){
            throw new ContactNullHandler(HttpStatus.BAD_REQUEST.toString());
        }
        if (department.getCountry()==null){
            throw new CountryNullHandler(HttpStatus.BAD_REQUEST.toString());
        }
    }

}
