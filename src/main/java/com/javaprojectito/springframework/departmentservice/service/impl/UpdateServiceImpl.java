package com.javaprojectito.springframework.departmentservice.service.impl;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.javaprojectito.springframework.departmentservice.dto.DepartmentDto;
import com.javaprojectito.springframework.departmentservice.entity.DepartmentEntity;
import com.javaprojectito.springframework.departmentservice.entity.generatorContact.FormattedCountryCode;
import com.javaprojectito.springframework.departmentservice.entity.generatorCountry.CapitalizeWords;
import com.javaprojectito.springframework.departmentservice.exception.badRequest.dataalreadyexist.ContactAlreadyExistHandler;
import com.javaprojectito.springframework.departmentservice.exception.badRequest.dataalreadyexist.EmailAlreadyExistHandler;
import com.javaprojectito.springframework.departmentservice.exception.badRequest.dataalreadyexist.NameAlreadyExistHandler;
import com.javaprojectito.springframework.departmentservice.repository.DepartmentRepository;
import com.javaprojectito.springframework.departmentservice.service.InvalidFormatService;
import com.javaprojectito.springframework.departmentservice.service.UpdateService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Objects;

@AllArgsConstructor
@Service
public class UpdateServiceImpl implements UpdateService {

    private DepartmentRepository departmentRepository;
    private InvalidFormatService invalidFormatService;
    @Override
    public String checkNameDept(DepartmentDto departmentDto, DepartmentEntity departmentEntity) {
        if(!(departmentDto.getDepartmentName()==null)){
            CapitalizeWords capitalizeWords = new CapitalizeWords();
            boolean department = departmentRepository.existsByDepartmentName(capitalizeWords.capitalizeWords(departmentDto.getDepartmentName()));
            if(!department){
                return departmentDto.getDepartmentName();
            }
            throw new NameAlreadyExistHandler(HttpStatus.BAD_REQUEST.toString());
        }
        return departmentEntity.getDepartmentName();
    }

    @Override
    public String checkEmailDept(DepartmentDto departmentDto, DepartmentEntity departmentEntity) {
        if(!(departmentDto.getEmail()==null)){
            boolean department = departmentRepository.existsByEmail(departmentDto.getEmail().toLowerCase());
            if(!department){
                return departmentDto.getDepartmentName();
            }
            throw new EmailAlreadyExistHandler(HttpStatus.BAD_REQUEST.toString());
        }
        return departmentEntity.getEmail();
    }

    @Override
    public String checkCountryDept(DepartmentDto departmentDto, DepartmentEntity departmentEntity) {
        if(!(departmentDto.getCountry()==null)){
            return departmentDto.getCountry();
        }
        return departmentEntity.getCountry();
    }
    @Override
    public String checkContactDept(DepartmentDto departmentDto, DepartmentEntity departmentEntity) {
        if(!(departmentDto.getContact()==null)){
            FormattedCountryCode formattedCountryCode = new FormattedCountryCode();
            if(Objects.equals(departmentEntity.getContact(), formattedCountryCode.contactFormatted(departmentEntity.getCountry(), departmentDto.getContact()))){
                throw new ContactAlreadyExistHandler(HttpStatus.BAD_REQUEST.toString());
            }
            invalidFormatService.errorContactInvalidFormat(departmentDto);
            return departmentDto.getContact();
        }

        String countryNick = "";
        Locale[] locales = Locale.getAvailableLocales();
        for (Locale locale : locales) {
            if (departmentEntity.getCountry().equals(locale.getDisplayCountry())) {
                countryNick=countryNick+locale.getCountry();
                break;
            }
        }

        PhoneNumberUtil phoneNumberUtil=PhoneNumberUtil.getInstance();
        String contactPhoneCode = "+"+phoneNumberUtil.getCountryCodeForRegion(countryNick);

        return departmentEntity.getContact().substring(contactPhoneCode.length());
    }


}
