package com.javaprojectito.springframework.departmentservice.service;

import com.javaprojectito.springframework.departmentservice.dto.DepartmentDto;

public interface DataNullService {
    void errorDataNull(DepartmentDto department);
}
