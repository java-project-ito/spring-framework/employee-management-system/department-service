package com.javaprojectito.springframework.departmentservice.entity.generatorId;

import java.util.*;

public class CustomIdGenerator {

    int countOfSpaceNameDept(String paramFromCreateDept){
        int counter = 0;

        for (int i = 0; i<paramFromCreateDept.length(); i++){
            if (Objects.equals(paramFromCreateDept.split("")[i], " ")){
                counter++;
            }
        }

        return counter;
    }
    String generatedNickNameDept(String paramFromCreateDept){
        int count = countOfSpaceNameDept(paramFromCreateDept);
        if(count==1){
            StringBuilder codeName = new StringBuilder();
            for(int i =0; i<paramFromCreateDept.length(); i++){
                if(i==0){
                    codeName.append(paramFromCreateDept.split("")[i]);
                    codeName.append(paramFromCreateDept.split("")[i + 1]);
                }else {
                    if(Objects.equals(paramFromCreateDept.split("")[i], " ")){
                        codeName.append(paramFromCreateDept.split("")[i + 1]);
                    }
                    if(Objects.equals(paramFromCreateDept.split("")[i], " ")){
                        codeName.append(paramFromCreateDept.split("")[i + 2]);
                    }
                }
            }

            return codeName.toString().toUpperCase();
        }if(count>=2 && count <=4){
            StringBuilder codeName = new StringBuilder();
            for(int i =0; i<paramFromCreateDept.length(); i++){
                if(i==0){
                    codeName.append(paramFromCreateDept.split("")[i]);
                }else {
                    if(Objects.equals(paramFromCreateDept.split("")[i], " ")){
                        codeName.append(paramFromCreateDept.split("")[i + 1]);
                    }
                }
            }
            return codeName.toString().toUpperCase();

        }if (count>4){
            StringBuilder codeName = new StringBuilder();
            List<String> stringList = Arrays.asList(paramFromCreateDept.split(" "));
            Collections.shuffle(stringList);
            for (int i = 0; i<5; i++){
                codeName.append(stringList.get(i));
            }
            return codeName.toString().toUpperCase();
        }

        return paramFromCreateDept.substring(0,3).toUpperCase();
    }


    public String idDept(String paramFromCreateDept){
        Random random = new Random();
        String randomVal = String.valueOf(random.nextInt(1,100));

        if(randomVal.length()==1){
            return generatedNickNameDept(paramFromCreateDept)+"-00"+randomVal;
        }
        if(randomVal.length()==2){
            return generatedNickNameDept(paramFromCreateDept)+"-0"+randomVal;
        }
        return generatedNickNameDept(paramFromCreateDept)+"-"+randomVal;
    }
}
