package com.javaprojectito.springframework.departmentservice.entity.generatorContact;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.javaprojectito.springframework.departmentservice.exception.badRequest.invalidformat.ContactPhoneTooLongHandler;
import org.springframework.http.HttpStatus;

import java.util.Locale;

public class FormattedCountryCode {
    public String contactFormatted(String country, String contact){
        String countryNick = "";
        Locale[] locales = Locale.getAvailableLocales();
        for (Locale locale : locales) {
            if (country.equals(locale.getDisplayCountry())) {
                countryNick=countryNick+locale.getCountry();
                break;
            }
        }

        PhoneNumberUtil phoneNumberUtil=PhoneNumberUtil.getInstance();
        String contactPhone = "+"+phoneNumberUtil.getCountryCodeForRegion(countryNick)+contact;
        if(contactPhone.length()>16){
            throw new ContactPhoneTooLongHandler(HttpStatus.BAD_REQUEST.toString());
        }
        return contactPhone;
    }
}
