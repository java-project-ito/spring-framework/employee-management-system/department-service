package com.javaprojectito.springframework.departmentservice.entity;

import jakarta.persistence.*;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "department")
public class DepartmentEntity {
    @Id
    @Column(name = "department_code", nullable = false)
    private String id;
    @Column(name = "department_name", nullable = false, unique = true)
    private String departmentName;
    @NotBlank(message = "Email is Mandatory")
    @Email(message = "Invalid Email")
    @Column(name = "department_email", unique = true)
    private String email;
    @NotBlank(message = "Phone is Mandatory")
    @Column(name = "department_phone", nullable = false, unique = true, length = 16)
    private String contact;
    @Column(name = "country", nullable = false)
    private String country;
}
