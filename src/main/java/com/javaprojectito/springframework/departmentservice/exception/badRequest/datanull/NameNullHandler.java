package com.javaprojectito.springframework.departmentservice.exception.badRequest.datanull;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NameNullHandler extends RuntimeException {
    private String error;

    public NameNullHandler(String error) {
        super(error);
    }
}
