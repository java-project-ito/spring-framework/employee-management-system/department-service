package com.javaprojectito.springframework.departmentservice.exception.badRequest.invalidformat;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ContactInvalidFormatHandler extends RuntimeException{
    private String status;

    public ContactInvalidFormatHandler(String status) {
        super(status);
    }
}
