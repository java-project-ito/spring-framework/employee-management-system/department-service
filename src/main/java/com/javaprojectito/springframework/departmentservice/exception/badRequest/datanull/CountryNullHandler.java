package com.javaprojectito.springframework.departmentservice.exception.badRequest.datanull;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CountryNullHandler extends RuntimeException{
    private String Status;
    public CountryNullHandler(String status){
        super(status);
    }
}
