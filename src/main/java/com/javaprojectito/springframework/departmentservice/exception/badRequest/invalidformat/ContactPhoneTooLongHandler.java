package com.javaprojectito.springframework.departmentservice.exception.badRequest.invalidformat;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONTINUE)
public class ContactPhoneTooLongHandler extends RuntimeException{
    private String status;

    public ContactPhoneTooLongHandler(String status){
        super(status);
    }
}
