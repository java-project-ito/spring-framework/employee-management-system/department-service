package com.javaprojectito.springframework.departmentservice.exception.badRequest.dataalreadyexist;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ContactAndEmailAlreadyExistHandler extends RuntimeException {
    private String error;

    public ContactAndEmailAlreadyExistHandler(String error) {
        super(error);
    }
}
