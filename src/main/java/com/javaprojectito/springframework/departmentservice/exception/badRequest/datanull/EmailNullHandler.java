package com.javaprojectito.springframework.departmentservice.exception.badRequest.datanull;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class EmailNullHandler extends RuntimeException {
    private String error;

    public EmailNullHandler(String error) {
        super(error);
    }
}
