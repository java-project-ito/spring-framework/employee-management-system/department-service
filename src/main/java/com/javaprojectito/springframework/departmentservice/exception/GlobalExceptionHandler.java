package com.javaprojectito.springframework.departmentservice.exception;

import com.javaprojectito.springframework.departmentservice.exception.badRequest.dataalreadyexist.*;
import com.javaprojectito.springframework.departmentservice.exception.badRequest.datanotfound.NotFoundHandler;
import com.javaprojectito.springframework.departmentservice.exception.badRequest.datanull.*;
import com.javaprojectito.springframework.departmentservice.exception.badRequest.invalidformat.ContactInvalidFormatHandler;
import com.javaprojectito.springframework.departmentservice.exception.badRequest.invalidformat.ContactPhoneTooLongHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NameAlreadyExistHandler.class)
    public ResponseEntity<ErrorDetails> nameDepartmentExistHandler(NameAlreadyExistHandler dataAlreadyExistHandler,
                                                                   WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                dataAlreadyExistHandler.getMessage(),
                "Name Department Already Exist",
                webRequest.getDescription(false)
        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmailAlreadyExistHandler.class)
    public ResponseEntity<ErrorDetails> emailDepartmentExistHandler(EmailAlreadyExistHandler emailAlreadyExistHandler,
                                                                    WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                emailAlreadyExistHandler.getMessage(),
                "Email Department Already Exist",
                webRequest.getDescription(false)
        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ContactAlreadyExistHandler.class)
    public ResponseEntity<ErrorDetails> phoneDepartmentExistHandler(ContactAlreadyExistHandler contactAlreadyExistHandler,
                                                                    WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                contactAlreadyExistHandler.getMessage(),
                "Phone Department Already Exist",
                webRequest.getDescription(false)
        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ContactAndEmailAlreadyExistHandler.class)
    public ResponseEntity<ErrorDetails> contactAndEmailDepartmentExistHandler(ContactAndEmailAlreadyExistHandler contactandEmailAlreadyExistHandler,
                                                                              WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                contactandEmailAlreadyExistHandler.getMessage(),
                "Phone And Email Department Already Exist",
                webRequest.getDescription(false)
        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ContactAndNameAlreadyExistHandler.class)
    public ResponseEntity<ErrorDetails> contactAndNameDepartmentExistHandler(ContactAndNameAlreadyExistHandler contactAndNameAlreadyExistHandler,
                                                                             WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                contactAndNameAlreadyExistHandler.getMessage(),
                "Phone And Name Department Already Exist",
                webRequest.getDescription(false)
        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmailAndNameAlreadyExistHandler.class)
    public ResponseEntity<ErrorDetails> emailAndNameDepartmentExistHandler(EmailAndNameAlreadyExistHandler emailAndNameAlreadyExistHandler,
                                                                           WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                emailAndNameAlreadyExistHandler.getMessage(),
                "Email And Name Department Already Exist",
                webRequest.getDescription(false)
        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AllDataAlreadyExistHandler.class)
    public ResponseEntity<ErrorDetails> allDataDepartmentExistHandler(AllDataAlreadyExistHandler allDataAlreadyExistHandler,
                                                                    WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                allDataAlreadyExistHandler.getMessage(),
                "Name, Email And Phone Department Already Exist",
                webRequest.getDescription(false)
        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NameNullHandler.class)
    public ResponseEntity<ErrorDetails> nameDepartmentNullHandler(NameNullHandler nameNullHandler,
                                                                   WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                nameNullHandler.getMessage(),
                "Name Department Shouldn't be Null",
                webRequest.getDescription(false)
        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmailNullHandler.class)
    public ResponseEntity<ErrorDetails> emailDepartmentNullHandler(EmailNullHandler emailNullHandler,
                                                                    WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                emailNullHandler.getMessage(),
                "Email Department Shouldn't be Null",
                webRequest.getDescription(false)
        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(ContactNullHandler.class)
    public ResponseEntity<ErrorDetails> phoneDepartmentNullHandler(ContactNullHandler contactNullHandler,
                                                                   WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                contactNullHandler.getMessage(),
                "Phone Department Shouldn't be Null",
                webRequest.getDescription(false)
        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CountryNullHandler.class)
    public ResponseEntity<ErrorDetails> countryNullHandler(CountryNullHandler countryNullHandler,
                                                           WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                countryNullHandler.getMessage(),
                "Country Department Shouldn't be Null",
                webRequest.getDescription(false)
        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(ContactInvalidFormatHandler.class)
    public ResponseEntity<ErrorDetails> contactInvalidFormatHandler(ContactInvalidFormatHandler contactInvalidFormatHandler,
                                                                   WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                contactInvalidFormatHandler.getMessage(),
                "Format Contact Phone Must be Number. Don't Input Your Code Country Phone. Just Input These Number After Country Code",
                webRequest.getDescription(false)

        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ContactPhoneTooLongHandler.class)
    public ResponseEntity<ErrorDetails> contactTooLongHandler(ContactPhoneTooLongHandler contactPhoneTooLongHandler,
                                                              WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                contactPhoneTooLongHandler.getMessage(),
                "Contact Too Long. Maximum Length is 16",
                webRequest.getDescription(false)

        );
        return new ResponseEntity<>(errorDetails,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundHandler.class)
    public ResponseEntity<ErrorDetails> notFoundHandler(NotFoundHandler notFoundHandler,
                                                        WebRequest webRequest){
        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                HttpStatus.BAD_REQUEST.toString(),
                notFoundHandler.getMessage(),
                webRequest.getDescription(false)
        );
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDetails> handleGlobalException(Exception exception,
                                                              WebRequest webRequest){

        ErrorDetails errorDetails = new ErrorDetails(
                LocalDateTime.now(),
                exception.getMessage(),
                "INTERNAL SERVER ERROR",
                webRequest.getDescription(false)
        );

        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
