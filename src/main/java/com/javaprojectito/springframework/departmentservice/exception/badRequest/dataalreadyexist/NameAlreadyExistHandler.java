package com.javaprojectito.springframework.departmentservice.exception.badRequest.dataalreadyexist;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NameAlreadyExistHandler extends RuntimeException {
    private String error;

    public NameAlreadyExistHandler(String error) {
        super(error);
    }
}
