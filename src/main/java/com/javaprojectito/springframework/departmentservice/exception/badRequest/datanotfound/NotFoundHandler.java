package com.javaprojectito.springframework.departmentservice.exception.badRequest.datanotfound;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NotFoundHandler extends RuntimeException{
    private String field;
    private String id;

    public NotFoundHandler(String field, String id){
        super(String.format("%s Not Found by %s", field, id));
        this.field=field;
        this.id=id;
    }
}
